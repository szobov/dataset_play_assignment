import logging
import os
import pathlib
import typing as _t
from dataclasses import dataclass

import click
import imageio

logger = logging.getLogger("dataset")

TIMESTAMPS_FILE_NAME = "per_observation_timestamps.txt"


class DatasetException(Exception):
    ...


@dataclass
class ItemsInfo:
    timestamps: _t.List[int]
    items: _t.List[_t.Any]


def parse_timestamp_file(file_path: pathlib.Path) -> _t.List[int]:
    with open(file_path) as f:
        return list(map(int, f.read().strip().split("\n")))


def get_files_list(*, path_to_dir: pathlib.Path,
                   file_prefix: str) -> _t.List[pathlib.Path]:
    files = (
        path_to_dir / name for name in os.listdir(path_to_dir)
        if os.path.isfile(path_to_dir / name) and name.startswith(file_prefix))
    return sorted(files, key=lambda s: int(s.stem.split("-")[1]))


def get_closest_index(timestamps: _t.List[int],
                      target_timestamp: int, current_index: int) -> int:
    number_of_timestamps = len(timestamps)
    if number_of_timestamps == 1:
        return 0

    current_timestamp = timestamps[current_index]

    if current_timestamp == target_timestamp:
        return current_index
    if current_timestamp < target_timestamp:
        next_index = current_index + 1
        while next_index <= number_of_timestamps:
            if next_index == number_of_timestamps:
                return next_index - 1
            if timestamps[next_index] < target_timestamp:
                next_index += 1
            else:
                break
        current_index = next_index

    if current_index == 0:
        return current_index

    diff_left = target_timestamp - timestamps[current_index - 1]
    diff_right = timestamps[current_index] - target_timestamp

    if diff_left < diff_right:
        return current_index - 1
    else:
        return current_index


def get_directory_data(path_to_dataset_dir: pathlib.Path, *,
                       dir_name: str, file_prefix: str) -> ItemsInfo:
    path_to_dir = path_to_dataset_dir / dir_name
    timestamps = parse_timestamp_file(path_to_dir / TIMESTAMPS_FILE_NAME)
    files = get_files_list(path_to_dir=path_to_dir, file_prefix=file_prefix)
    if len(timestamps) != len(files):
        raise DatasetException("Number of timestamps and files for "
                               f"{dir_name} should be the same")
    return ItemsInfo(timestamps=timestamps, items=files)


def get_frames_data(path_to_dataset_dir: pathlib.Path, *,
                    dir_name: str, video_file_name: str) -> ItemsInfo:

    path_to_dir = path_to_dataset_dir / dir_name

    timestamps = parse_timestamp_file(path_to_dir / TIMESTAMPS_FILE_NAME)
    frames_reader = imageio.get_reader(path_to_dir / video_file_name)
    if frames_reader.count_frames() != len(timestamps):
        raise DatasetException("Number of timestamps and frames for "
                               f"{dir_name} should be the same")
    return ItemsInfo(timestamps=timestamps, items=frames_reader)


def get_dataset_iterator(path_to_dataset_dir: pathlib.Path
                         ) -> _t.Iterator[int, _t.Any, _t.Any, _t.Any]:

    touch = get_directory_data(
        path_to_dataset_dir, dir_name="touch", file_prefix="observation")

    depth = get_directory_data(
        path_to_dataset_dir, dir_name="depth", file_prefix="frame")

    rgb = get_frames_data(
        path_to_dataset_dir, dir_name="rgb", video_file_name="video.mp4")

    current_depth_index = 0
    current_rgb_index = 0

    for touch_timestamp, touch_item in zip(touch.timestamps, touch.items):
        current_depth_index = get_closest_index(
            depth.timestamps, touch_timestamp, current_depth_index)
        current_depth_index = get_closest_index(
            rgb.timestamps, touch_timestamp, current_rgb_index)

        yield (touch_timestamp, touch_item,
               rgb.items.get_data(current_rgb_index),
               depth.items[current_depth_index])


def run(dataset_dir: pathlib.Path) -> None:
    for (timestamp, touch, rgb, depth) in get_dataset_iterator(dataset_dir):
        print("")


@click.command()
@click.option('-d',
              '--dataset-dir',
              help="Path to dataset directory",
              default="./dataset", show_default=True,
              type=click.Path(exists=True, resolve_path=True))
def main(dataset_dir: str) -> None:
    run(pathlib.Path(dataset_dir))


if __name__ == '__main__':
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    main()
