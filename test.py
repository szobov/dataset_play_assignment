import pathlib

import pytest

from .main import get_closest_index, get_dataset_iterator


def test_timestamp_iter():
    timestamps = [1, 10, 13, 20]
    current_index = 0
    current_index = get_closest_index(timestamps, 0, current_index)
    assert current_index == 0
    current_index = get_closest_index(timestamps, 2, current_index)
    assert current_index == 0
    current_index = get_closest_index(timestamps, 6, current_index)
    assert current_index == 1
    current_index = get_closest_index(timestamps, 9, current_index)
    assert current_index == 1
    current_index = get_closest_index(timestamps, 11, current_index)
    assert current_index == 1
    current_index = get_closest_index(timestamps, 13, current_index)
    assert current_index == 2
    current_index = get_closest_index(timestamps, 21, current_index)
    assert current_index == 3


def test_get_dataset_iterator():
    path_to_dataset = pathlib.Path("./dataset").resolve()
    assert [] != list(get_dataset_iterator(path_to_dataset))
